---
title: "Ma page de recommandations"
order: 0
in_menu: true
---
Voici la liste de mes logiciels libres préférés, je vous les recommande tout particulièrement !

## Cartographie des formations


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Freeplane.png">
    </div>
    <div>
      <h2>Freeplane</h2>
      <p>Pour créer des cartes heuristiques, des cartes mentales (mind mapping). Une référence dans cette catégorie.</p>
      <div>
        <a href="https://framalibre.org/notices/freeplane.html">Vers la notice Framalibre</a>
        <a href="https://www.freeplane.org">Vers le site</a>
      </div>
    </div>
  </article>



  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Kanboard.png">
    </div>
    <div>
      <h2>Kanboard</h2>
      <p>Kanboard, un logiciel libre pour gérer ses projets avec la méthode Kanban</p>
      <div>
        <a href="https://framalibre.org/notices/kanboard.html">Vers la notice Framalibre</a>
        <a href="https://kanboard.org">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Easy!%20Appointments.jpeg">
    </div>
    <div>
      <h2>Easy! Appointments</h2>
      <p>Prise de rendez-vous, organisation d'évènements, inscriptions à des prestations individuelles ou collective...</p>
      <div>
        <a href="https://framalibre.org/notices/easy-appointments.html">Vers la notice Framalibre</a>
        <a href="https://easyappointments.org/">Vers le site</a>
      </div>
    </div>
  </article>

## Travailler sur les PDF

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/PDFtk.png">
    </div>
    <div>
      <h2>PDFtk</h2>
      <p>La boîte à outil ultime pour modifier les fichiers PDF.</p>
      <div>
        <a href="https://framalibre.org/notices/pdftk.html">Vers la notice Framalibre</a>
        <a href="https://www.pdflabs.com/tools/pdftk-server/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Xournal++.png">
    </div>
    <div>
      <h2>Xournal++</h2>
      <p>Pour prendre notes manuscrites et annoter des pdf sur tablette graphique.</p>
      <div>
        <a href="https://framalibre.org/notices/xournal.html">Vers la notice Framalibre</a>
        <a href="https://github.com/xournalpp/xournalpp">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Krop.png">
    </div>
    <div>
      <h2>Krop</h2>
      <p>Ouitl de découpage de PDF. Permet, notamment, de ne sélectionner qu'une partie d'une page PDF (enlever les publicités par exemple), redimensionner un fichier PDF pour l'imprimer sans perte de papier…</p>
      <div>
        <a href="https://framalibre.org/notices/krop.html">Vers la notice Framalibre</a>
        <a href="https://arminstraub.com/software/krop">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/PDF4Teachers.png">
    </div>
    <div>
      <h2>PDF4Teachers</h2>
      <p>PDF4Teachers est une application d'édition de documents PDF libre et open source, permettant la correction de copies PDF de manière optimisée.</p>
      <div>
        <a href="https://framalibre.org/notices/pdf4teachers.html">Vers la notice Framalibre</a>
        <a href="https://pdf4teachers.org/">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/BookletImposer.svg">
    </div>
    <div>
      <h2>BookletImposer</h2>
      <p>Créer des livrets pour imprimer des PDF en multipages (4 pages sur une seule feuille par exemple)</p>
      <div>
        <a href="https://framalibre.org/notices/bookletimposer.html">Vers la notice Framalibre</a>
        <a href="https://kjo.herbesfolles.org/bookletimposer/">Vers le site</a>
      </div>
    </div>
  </article>

## Travail sur les images

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Inkscape.png">
    </div>
    <div>
      <h2>Inkscape</h2>
      <p>Un puissant logiciel de dessin vectoriel.</p>
      <div>
        <a href="https://framalibre.org/notices/inkscape.html">Vers la notice Framalibre</a>
        <a href="https://inkscape.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/GIMP.png">
    </div>
    <div>
      <h2>GIMP</h2>
      <p>Un puissant logiciel d'édition et de retouche d'images.</p>
      <div>
        <a href="https://framalibre.org/notices/gimp.html">Vers la notice Framalibre</a>
        <a href="https://www.gimp.org/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Sozi.png">
    </div>
    <div>
      <h2>Sozi</h2>
      <p>Créer des présentations dynamiques par effet de zoom à partir d'une image svg.</p>
      <div>
        <a href="https://framalibre.org/notices/sozi.html">Vers la notice Framalibre</a>
        <a href="http://sozi.baierouge.fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/ImageOptim.png">
    </div>
    <div>
      <h2>ImageOptim</h2>
      <p>Un outil pour optimiser la taille de vos photos et images sans perdre en qualité</p>
      <div>
        <a href="https://framalibre.org/notices/imageoptim.html">Vers la notice Framalibre</a>
        <a href="https://imageoptim.com/mac">Vers le site</a>
      </div>
    </div>
  </article>

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Krita.png">
    </div>
    <div>
      <h2>Krita</h2>
      <p>Logiciel de création de dessins, peintures, logos et animations</p>
      <div>
        <a href="https://framalibre.org/notices/krita.html">Vers la notice Framalibre</a>
        <a href="https://krita.org/">Vers le site</a>
      </div>
    </div>
  </article>



## Gestion des ressources documentaires

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Calibre.png">
    </div>
    <div>
      <h2>Calibre</h2>
      <p>Gestionnaire de bibliothèque de livres numériques.</p>
      <div>
        <a href="https://framalibre.org/notices/calibre.html">Vers la notice Framalibre</a>
        <a href="https://calibre-ebook.com/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

## Outils de bureautique


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/LibreOffice.png">
    </div>
    <div>
      <h2>LibreOffice</h2>
      <p>Une suite bureautique issue d'OpenOffice.org.</p>
      <div>
        <a href="https://framalibre.org/notices/libreoffice.html">Vers la notice Framalibre</a>
        <a href="https://fr.libreoffice.org/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Thunderbird.png">
    </div>
    <div>
      <h2>Thunderbird</h2>
      <p>Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/thunderbird.html">Vers la notice Framalibre</a>
        <a href="https://www.thunderbird.net/fr/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Firefox.png">
    </div>
    <div>
      <h2>Firefox</h2>
      <p>Le navigateur web épris de liberté distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/firefox.html">Vers la notice Framalibre</a>
        <a href="https://www.mozilla.org/fr/firefox/">Vers le site</a>
      </div>
    </div>
  </article>

## Communication

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Signal.png">
    </div>
    <div>
      <h2>Signal</h2>
      <p>Application de messagerie et téléphonie mobile respectueuse de la vie privée.</p>
      <div>
        <a href="https://framalibre.org/notices/signal.html">Vers la notice Framalibre</a>
        <a href="https://signal.org">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Mastodon.png">
    </div>
    <div>
      <h2>Mastodon</h2>
      <p>Un logiciel décentralisé et fédéré de microblogage en 500 caractères.</p>
      <div>
        <a href="https://framalibre.org/notices/mastodon.html">Vers la notice Framalibre</a>
        <a href="https://joinmastodon.org/">Vers le site</a>
      </div>
    </div>
  </article>

## Veille documentaire


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Shaarli.png">
    </div>
    <div>
      <h2>Shaarli</h2>
      <p>Un logiciel léger pour sauvegarder, trier et partager des adresses web.</p>
      <div>
        <a href="https://framalibre.org/notices/shaarli.html">Vers la notice Framalibre</a>
        <a href="https://github.com/shaarli/Shaarli">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FreshRSS.png">
    </div>
    <div>
      <h2>FreshRSS</h2>
      <p>FreshRSS permet de s'abonner à des sites pour suivre l'actualité.</p>
      <div>
        <a href="https://framalibre.org/notices/freshrss.html">Vers la notice Framalibre</a>
        <a href="https://freshrss.org">Vers le site</a>
      </div>
    </div>
  </article>

## Pour ce que j'aurai filmé

<article class="framalibre-notice">
  <div>
    <img src="https://beta.framalibre.org/images/logo/PeerTube.png">
  </div>
  <div>
    <h2>PeerTube</h2>
    <p>PeerTube est un logiciel décentralisé et fédéré d'hébergement de vidéos.</p>
    <div>
      <a href="https://beta.framalibre.org/notices/peertube.html">Vers la notice Framalibre</a>
      <a href="https://joinpeertube.org/fr/">Vers le site</a>
    </div>
  </div>
</article>

## Pour écouter des émissions

Quand je fais une pause, j'aime bien écouter un petit podcast avec :

  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/AntennaPod.png">
    </div>
    <div>
      <h2>AntennaPod</h2>
      <p>Un gestionnaire de Podcast pour Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/antennapod.html">Vers la notice Framalibre</a>
        <a href="http://antennapod.org/">Vers le site</a>
      </div>
    </div>
  </article> 